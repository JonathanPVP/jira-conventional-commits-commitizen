import re
from pathlib import Path
from typing import Dict

from commitizen import defaults  # pyright: ignore
from commitizen.cz.base import BaseCommitizen  # pyright: ignore
from commitizen.cz.utils import (  # pyright: ignore
    multiple_line_breaker,  # pyright: ignore
    required_validator,  # pyright: ignore
)
from commitizen.defaults import Questions  # pyright: ignore

__all__ = ["JiraConventionalCommitsCz"]


def parse_scope(text: str) -> str:
    if not text:
        return ""

    scope = text.strip().split()
    if len(scope) == 1:
        return scope[0]

    return "-".join(scope)


def parse_subject(text: str) -> str:
    text = text.strip(".").strip()
    return required_validator(text, msg="Subject is required.")


def parse_jira_ticket_number(text: str) -> str:
    return text.upper()


JIRA_TICKET_NUMBER_PATTERN = r"(?:\[[A-Z]{2,}-[0-9]+\] )?"


def prefix_jira_ticket_number_to_pattern(pattern: str) -> str:
    return r"^" + JIRA_TICKET_NUMBER_PATTERN + pattern[1:]


class JiraConventionalCommitsCz(BaseCommitizen):
    bump_pattern = defaults.bump_pattern[1:]
    bump_map = defaults.bump_map
    commit_parser = prefix_jira_ticket_number_to_pattern(defaults.commit_parser)
    version_parser = defaults.version_parser
    change_type_map = {
        "feat": "Feat",
        "fix": "Fix",
        "refactor": "Refactor",
        "perf": "Perf",
    }
    changelog_pattern = prefix_jira_ticket_number_to_pattern(defaults.bump_pattern)

    def questions(self) -> Questions:
        questions: Questions = [
            {
                "type": "input",
                "name": "jira_ticket_number",
                "message": (
                    "What is the Jira ticker number of this change? (press [enter] to skip)\n"
                ),
                "filter": parse_jira_ticket_number,
            },
            {
                "type": "list",
                "name": "prefix",
                "message": "Select the type of change you are committing",
                "choices": [
                    {
                        "value": "fix",
                        "name": "fix: A bug fix. Correlates with PATCH in SemVer",
                        "key": "x",
                    },
                    {
                        "value": "feat",
                        "name": "feat: A new feature. Correlates with MINOR in SemVer",
                        "key": "f",
                    },
                    {
                        "value": "docs",
                        "name": "docs: Documentation only changes",
                        "key": "d",
                    },
                    {
                        "value": "style",
                        "name": (
                            "style: Changes that do not affect the "
                            "meaning of the code (white-space, formatting,"
                            " missing semi-colons, etc)"
                        ),
                        "key": "s",
                    },
                    {
                        "value": "refactor",
                        "name": (
                            "refactor: A code change that neither fixes "
                            "a bug nor adds a feature"
                        ),
                        "key": "r",
                    },
                    {
                        "value": "perf",
                        "name": "perf: A code change that improves performance",
                        "key": "p",
                    },
                    {
                        "value": "test",
                        "name": "test: Adding missing or correcting existing tests",
                        "key": "t",
                    },
                    {
                        "value": "build",
                        "name": (
                            "build: Changes that affect the build system or "
                            "external dependencies (example scopes: pip, docker, npm)"
                        ),
                        "key": "b",
                    },
                    {
                        "value": "ci",
                        "name": (
                            "ci: Changes to our CI configuration files and "
                            "scripts (example scopes: GitLabCI)"
                        ),
                        "key": "c",
                    },
                ],
            },
            {
                "type": "input",
                "name": "scope",
                "message": (
                    "What is the scope of this change? (class or file name): (press [enter] to skip)\n"
                ),
                "filter": parse_scope,
            },
            {
                "type": "input",
                "name": "subject",
                "filter": parse_subject,
                "message": (
                    "Write a short and imperative summary of the code changes: (lower case and no period)\n"
                ),
            },
            {
                "type": "input",
                "name": "body",
                "message": (
                    "Provide additional contextual information about the code changes: (press [enter] to skip)\n"
                ),
                "filter": multiple_line_breaker,
            },
            {
                "type": "confirm",
                "message": "Is this a BREAKING CHANGE? Correlates with MAJOR in SemVer",
                "name": "is_breaking_change",
                "default": False,
            },
            {
                "type": "input",
                "name": "footer",
                "message": (
                    "Footer. Information about Breaking Changes and "
                    "reference issues that this commit closes: (press [enter] to skip)\n"
                ),
            },
        ]
        return questions

    def message(self, answers: Dict[str, str]) -> str:
        jira_ticket_number = answers["jira_ticket_number"]
        prefix = answers["prefix"]
        scope = answers["scope"]
        subject = answers["subject"]
        body = answers["body"]
        footer = answers["footer"]
        is_breaking_change = answers["is_breaking_change"]

        if jira_ticket_number:
            jira_ticket_number = f"[{jira_ticket_number}] "
        if scope:
            scope = f"({scope})"
        if body:
            body = f"\n\n{body}"
        if is_breaking_change:
            scope += "!"
            if footer:
                footer = f"BREAKING CHANGE: {footer}"
        if footer:
            footer = f"\n\n{footer}"

        return f"{jira_ticket_number}{prefix}{scope}: {subject}{body}{footer}"

    def example(self) -> str:
        return (
            "[ABC-123] fix: correct minor typos in code\n"
            "\n"
            "see the issue for details on the typos fixed\n"
            "\n"
            "closes issue #12"
        )

    def schema(self) -> str:
        return (
            "([<jira-ticket-number>] )\n"
            "<type>(<scope>)(!): <subject>\n"
            "<BLANK LINE>\n"
            "<body>\n"
            "<BLANK LINE>\n"
            "(BREAKING CHANGE: )<footer>"
        )

    def schema_pattern(self) -> str:
        return (
            r"(?s)"  # To explictly make . match new line
            + JIRA_TICKET_NUMBER_PATTERN
            + r"(build|ci|docs|feat|fix|perf|refactor|style|test|chore|revert|bump)"  # type
            r"(\(\S+\))?!?:"  # scope
            r"( [^\n\r]+)"  # subject
            r"((\n\n.*)|(\s*))?$"
        )

    def info(self) -> str:
        dir_path = Path(__file__).parent
        filepath = dir_path / "jira_conventional_commits_info.txt"
        return filepath.read_text()

    def process_commit(self, commit: str) -> str:
        pat = re.compile(self.schema_pattern())
        m = re.match(pat, commit)
        if m is None:
            return ""
        return m.group(3).strip()


discover_this = JiraConventionalCommitsCz  # used by the plug-in system
