from setuptools import setup

setup(
    name="jira-conventional-commits-commitizen",
    version="0.1.1",
    author="Jonathan Plasse",
    author_email="jonathan.plasse@drotek.com",
    py_modules=["cz_jira_conventional_commits"],
    license="MIT",
    long_description="Jira Conventional Commits Commitizen",
    install_requires=["commitizen"],
)
